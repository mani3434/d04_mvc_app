﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace D04.Controllers
{
    public class ManiController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult JSON()
        {
            return Json(new
            {
                apptype = ".NET Core",
                appname = "MVC"
            });
        }
        public IActionResult XML()
        {
            return Content("<xml>My data</xml>", "text/xml");
        }

        public IActionResult HTML()
        {
            return Content("<!DOCTYPE html><html><body>Hello World</body></html>", "text/html");
        }

        public IActionResult Bad()
        {
            return NotFound("Returns 404 not found. Right-click browser Inspect / Console to verify");
        }

        //How do we call this?
        public IActionResult Get(int id)
        {
            return Content("{_id: " + id + "}, {Name: 'Stanley'}", "application/json");
        }

        public IActionResult MORE()
        {
            return Redirect("http://hamidmosalla.com/2017/03/29/asp-net-core-action-results-explained");
        }


    }
}
